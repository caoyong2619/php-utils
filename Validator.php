<?php

/**
 * 验证器
* @author 曹勇
* @example
* $data = array('username' => 'caoyong','password' => '');
* $rules = array('username' => 'require','password' => 'require');
* $validator = new Validator($data,$rules);
* $is_pass = $validator->passed();
* $is_fail = $validator->failed();
* $message = $validator->messages();
*/

class Validator
{
	/**
	 * 待验证数据
	 * @var array
	 */
	protected $data;

	/**
	 * 验证规则
	 * @var array
	 */
	protected $rule;

	/**
	 * 错误信息
	 * @var Messages
	 */
	protected $messages;

	/**
	 * 自定义错误信息
	 * @var array
	 */
	protected $custom_messages;

	/**
	 * 扩展规则
	 * @var array
	 */
	protected static $extensions = array();

	public function __construct(array $data,array $rule,array $messages = array())
	{
		$this->setData($data);
		$this->setRule($rule);
		$this->setMessages($messages);
		$this->messages = new Messages();
	}

	public function setData(array $data)
	{
		$this->data = $data;
	}

	public function setRule(array $rule)
	{
		$this->rule = $rule;
	}

	public function setMessages(array $messages)
	{
		$this->custom_messages = $messages;
	}

	protected function validate($attr,$rule)
	{
		if (is_array($rule))
		{
			foreach ($rule as $v)
			{
				if(false === $this->validate($attr, $v))
					break;
			}
		}
		else
		{
			list($rule,$args) = $this->parseRule($rule);
				
			$method = 'validate'.$rule;
				
			$args = array_merge(array($attr,$this->getValue($attr)),$args);
				
			$result =  call_user_func_array(array($this,$method), $args);
				
			if (false === $result)
			{
				$rule = lcfirst($rule);
				if (isset($this->custom_messages[$attr]))
				{
					if (is_array($this->custom_messages[$attr]) && isset($this->custom_messages[$attr][$rule]))
					{
						$message = $this->custom_messages[$attr][$rule];
					}
					else
						if ($this->custom_messages[$attr])
						{
							$message = $this->custom_messages[$attr];
						}
					else
					{
						$message = $attr.' return failed in rule '.$rule;
					}
				}
				else
					$message = $attr.' return failed in rule '.$rule;
				$this->messages[$attr] = $message;
			}
			return $result;
		}
	}

	public function passed()
	{
		foreach ($this->rule as $attr => $rule)
		{
			$this->validate($attr, $rule);
		}
		return 0 === count($this->messages);
	}

	public function failed()
	{
		return !$this->passed();
	}

	public function messages($key = null)
	{
		if ($key && isset($this->messages[$key]))
			return $this->messages[$key];
		return $this->messages;
	}

	protected function parseRule($rule)
	{
		if (false !== strpos($rule,'|'))
		{
			list($rulename,$args) = explode('|', $rule);
			$args = explode(':', $args);
		}
		else
		{
			$rulename = $rule;
			$args = array();
		}
		return array(ucfirst($rulename),$args);
	}

	protected function getValue($attr)
	{
			return isset( $this->data[$attr]) ?  $this->data[$attr] : null;
	}

	/**
	 * 扩展验证规则
	 * @param string $name
	 * @param Closure $rule
	 */
	public static function addExtension($name,Closure $rule)
	{
		static::$extensions[$name] = $rule;
	}

	/**
	 *  批量增加扩展规则
	 *  @param $rules array
	 */

	public static function addExtensions(array $rules)
	{
		foreach ($rules as $k => $v)
		{
			static::addExtenstion($k, $v);
		}
	}

	public function __call($method,$args)
	{
		$method = lcfirst(substr($method, 8));

		$args = array_merge(array($this),$args);

		if (isset(static::$extensions[$method]))
		{
			return call_user_func_array(static::$extensions[$method], $args);
		}

		throw new \Exception('rule '.$method.' dose not exits');
	}

	protected function validateRequired($attr,$value)
	{
		return !empty($value);
	}

	protected function validateLength($attr,$value,$len)
	{
		return  $len == $min;
	}

	protected function validateMin($attr,$value,$len)
	{
		return strlen($value) > $len;
	}

	protected function validateMax($attr,$value,$len)
	{
		return strlen($value) < $len;
	}

	protected function ValidateBetween($attr,$value,$min,$max)
	{
		return $this->validateMin($attr, $value, $min) && $this->validateMax($attr, $value, $max);
	}

	protected function validateEmail($attr,$value)
	{
		$regex = '/[\w!#$%&\'*+\/=?^_`{|}~-]+(?:\.[\w!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/i';

		return (bool)preg_match($regex, $value);
	}

	protected function validateNumber($attr,$value)
	{
		return is_numeric($value);
	}

	protected function validateIn($attr,$value,$in_data)
	{
		$in_data = explode(',', $in_data);
		return in_array($value, $in_data);
	}

	protected function validateNotin($attr,$value,$in_data)
	{
		return !$this->validateIn($attr, $value, $in_data);
	}

	protected function validateEq($attr,$value,$eq)
	{
		return $value == $eq;
	}

	protected function validateConfirm($attr,$value,$confirm)
	{
		return $this->validateEq($attr, $value, $this->getValue($confirm));
	}

	protected function validateUrl($attr,$value)
	{
		$regex = '/[a-zA-z]+://[^\s]*/i';
		return (bool)preg_match($regex, $value);
	}

	protected function validateMobile($attr,$value)
	{
		return preg_match('/1(3|4|5|8)\d{9}/',$value);
	}

	protected function validateQQ($attr,$value)
	{
		return preg_match('/\d{5,}/', $value);
	}
	
	protected function validateNotNull($attr,$value)
	{
		return !is_null($value);
	}
}

class Messages implements \ArrayAccess,\Iterator,\Countable,\JsonSerializable
{
	protected $items;
	
	public function __construct($items = array())
	{
		$this->items = $items;
	}
	
	public function current () 
	{
		return current($this->items);
	}
	
	public function next () 
	{
		return next($this->items);
	}
	
	public function key () 
	{
		return key($this->items);
	}
	
	public function valid () 
	{
		return $this->current() !== false;
	}
	
	public function rewind () 
	{
		reset($this->items);
	}
	
	public function offsetExists ($offset) 
	{
		return isset($this->items[$offset]);
	}

	public function offsetGet ($offset) 
	{
		return isset($this->items[$offset]) ? $this->items[$offset] : null;
	}

	public function offsetSet ($offset, $value) 
	{
		if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
	}

	public function offsetUnset ($offset) 
	{
		unset($this->items[$offset]);
	}
	
	public function jsonSerialize()
	{
		return $this->items;
	}

	public function first()
	{
		reset($this->items);
		return current($this->items);
	}
	
	public function count()
	{
		return count($this->items);
	}
}