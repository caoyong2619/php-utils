<?php
class Event
{
	public static $events = array();
	
	public static function listen($name,Closure $callback)
	{
		static::$events[$name][] = $callback;
	}
	
	public static function trigger($name)
	{
		if (!isset(static::$events[$name]))
			throw new Exception('event '.$name.' does not exists');
		
		$args = func_get_args();
		array_shift($args);
		foreach (static::$events[$name] as $event)
		{
			/**
			 * event中返回false时，终止事件触发
			 */
			if ($result = call_user_func_array($event, $args))
			{
				if (!is_null($result))
					return $result;
				
				if (false === $result)
					return $result;
					break;
			}
		}
		return $result;
	}
}