<?php
/**
 * 
 * @author 曹勇
 *
 */
class Field
{
	protected $attribues = [];
	protected $value;
	protected $type;
	protected $name;
	protected $default_value;
	
	public function __construct($name,$type,$default = null)
	{
		$this->name = $name;
		$this->type = $type;
		$this->default_value = $default;
		$this->init();
	}
	
	public function init()
	{
		
	}
	
	public function getValue()
	{
		return is_null($this->value) ? (is_null($this->default_value) ? null : $this->default_value) : $this->value;
	}
	
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function setAttributes(array $attributes)
	{
		$this->attribues = $attributes;
		return $this;
	}
	
	public function get()
	{
		return array_merge(['name' => $this->getName(),'type' => $this->getType(),'value' => $this->getValue()],$this->attribues);
	}
}